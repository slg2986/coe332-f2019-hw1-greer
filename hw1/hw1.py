def read_data():

	import csv
	perfect_dict=[]
	with open('rainfall.csv') as csvfile:
		reader = list(csv.reader(csvfile))
		for row in reader[1:]:
			temp_dict = {}
			temp_dict["id"] = int(row[0])
			temp_dict["year"] = int(row[1])
			temp_dict["rain"] = float(row[2])
			perfect_dict.append(temp_dict)
	return(perfect_dict)
	
	# return [ ]

def dates(data, start=None, end=None):

	if start == None and end == None:
		return(data)
	if start != None and end == None:
		return(list(filter(lambda x : x['year'] >= start, data)))
	if start == None and end != None:
		return(list(filter(lambda x : x['year'] <= end, data)))
	if start != None and end != None:
		return(list(filter(lambda x : x['year'] >= start and x['year'] <= end, data)))

	# return [ ]

def paginate(data, offset=None, limit=None):

	if offset == None and limit == None:
		return(data)
	if offset != None and limit == None:
		return data[offset:None]
	if offset == None and limit != None:
		return data[None:limit]
	if offset != None and limit != None:
		return data [offset:(offset+limit)]

	# return [ ]
